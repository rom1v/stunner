use anyhow::{anyhow, bail, Result};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use stunclient::StunClient;
use tokio::net::UdpSocket;

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<_> = std::env::args().collect();
    if args.len() != 2 {
        bail!("Syntax error, expected: {} <port>", args[0]);
    }

    let port = args[1].parse()?;

    let bind_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::UNSPECIFIED), port);
    let addr_iter = tokio::net::lookup_host("stun.l.google.com:19302").await?;
    let stun_server_addr = addr_iter
        .filter(|x| x.is_ipv4())
        .next().ok_or_else(|| anyhow!("No stun server address found"))?;
    let udp = UdpSocket::bind(&bind_addr).await?;

    let client = StunClient::new(stun_server_addr);
    let addr = client.query_external_address_async(&udp).await.unwrap();
    println!("{addr}");
    Ok(())
}
